'use strict';


  
let vm = new Vue({
    el: '#container',
    data() {
     
      return {
        info: null,
        modalVisibility:false,
        
      };
    },
    methods:{
        Status(info){
        for(let j in info){
            if(info[j].status="Pending"){
                console.log(info[j].status)
                $('#status').css({'background':'yellow'})
            }
        }
        return this.info;  
        },
        value(){
            let name, secondName,accountName;
            name = $('#name').val();
            secondName = $('#name2').val();
            accountName = name+'.'+secondName;
                $('#name,#name2').blur(function(){
                $('#accountNames').html(accountName)
               
              })
        },
        
        emailValidation(info){
            let _this = this;
            let email = $('#email').val();
            let regEmail = email.match(/^[a-zA-Z0-9\.\_]{2,30}@{1}\w{1,30}\.{1}\w{2,4}\.{0,1}\w{0,4}$/);
            $('#email').focus(function(){
                $('#email').removeClass('error');
                $('#emailError').html('')
            })
          
            $('#save').click(function(e){
                for(var i in info){
                    if(email==info[i].email){
                        $('#emailError').html('*This e-mail already exist');
                        break;      
                    }
                    if(regEmail===null&&i>=info.length-1){
                        $('#email').addClass('error');
                        $('#emailError').html('*Invalid e-mail')
                    }
                    else if(regEmail!==null&&i>=info.length-1){
                    $('#email').removeClass('error');
                    $('#emailError').html('');
                    let user = {
                        email:email
                    };
                    info.push(user)

                    return _this.modalVisibility = false;    
                }

            }
       
        })


        },
        timeConverter(UNIX_timestamp){
            var a = new Date(UNIX_timestamp * 1000);
            var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
            var year = a.getFullYear();
            var month = months[a.getMonth()];
            var date = a.getDate();
          
            var time = date + ' ' + month + ' ' + year + '';
            return time;
          },
        
    },

    mounted() {
      axios
        .get('http://localhost:3000/employees')
        .then(response => (this.info = response.data));
        
    }
  });
